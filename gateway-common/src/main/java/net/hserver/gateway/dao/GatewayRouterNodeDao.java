package net.hserver.gateway.dao;

import net.hserver.gateway.entity.GateWayRouterNode;
import net.hserver.plugins.beetlsql.annotation.BeetlSQL;
import org.beetl.sql.mapper.BaseMapper;

/**
 * @author hxm
 */
@BeetlSQL
public interface GatewayRouterNodeDao extends BaseMapper<GateWayRouterNode> {

}
