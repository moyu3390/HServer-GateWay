package net.hserver.gateway.config;

import top.hserver.core.interfaces.GlobalException;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.server.context.Webkit;
import top.hserver.core.server.util.ExceptionUtil;
import top.hserver.core.server.util.JsonResult;

@Bean
public class WebException implements GlobalException {

  @Override
  public void handler(Throwable throwable, int httpStatusCode, String errorDescription, Webkit webkit) {
    throwable.printStackTrace();
    System.out.println(webkit.httpRequest.getUri() + "--->" + throwable.getMessage());
    JsonResult result =JsonResult.error("发生了一个错误");
    result.put("success",false);
    result.put("data", ExceptionUtil.getMessage(throwable));
    webkit.httpResponse.sendJson(result);
  }
}