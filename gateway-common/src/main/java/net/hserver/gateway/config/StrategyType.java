package net.hserver.gateway.config;

public enum StrategyType {

  轮询法(1,"轮询法"),
  随机法(2,"随机法"),
  源地址哈希法(3,"源地址哈希法"),
  加权轮询法(4,"加权轮询法"),
  最小连接数法(5,"最小连接数法");

  private Integer type;
  private String name;

  StrategyType(Integer type, String name) {
    this.type = type;
    this.name = name;
  }



  public Integer getType() {
    return type;
  }

  public String getName() {
    return name;
  }

}
