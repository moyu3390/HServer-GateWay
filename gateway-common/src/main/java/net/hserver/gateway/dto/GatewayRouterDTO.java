package net.hserver.gateway.dto;

import lombok.Data;

/**
 * @author hxm
 */
@Data
public class GatewayRouterDTO {

  private String id;
  private String desc;
  private String url;
  private String nodeIds;
  private Integer strategyType;

}
