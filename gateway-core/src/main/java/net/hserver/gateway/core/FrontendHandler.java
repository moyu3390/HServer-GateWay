package net.hserver.gateway.core;

import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import net.hserver.gateway.core.bean.Proxy;

import java.lang.reflect.Method;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * @author hxm
 */
public class FrontendHandler extends ChannelInboundHandlerAdapter {
    private static Method getTtlExecutor = null;
    public Channel outboundChannel;

    static void closeOnFlush(Channel ch) {
        if (ch.isActive()) {
            ch.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
        }
    }

    public void read(Proxy proxy, Object msg) {
        System.out.println(msg);
        if (msg instanceof FullHttpRequest) {
            FullHttpRequest fullHttpRequest = (FullHttpRequest) msg;
            HttpHeaders headers = fullHttpRequest.headers();
            headers.set("Host", proxy.getServerNode().getInetSocketAddress().getHostString());
            outboundChannel.writeAndFlush(msg);
        } else if (msg instanceof WebSocketFrame) {
            System.out.println(msg);
            outboundChannel.writeAndFlush(msg);
        } else {
            closeOnFlush(proxy.getCtx().channel());
        }
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, Object msg) throws Exception {
        if (getTtlExecutor == null) {
            getTtlExecutor = ctx.executor().getClass().getMethod("getTtlExecutor");
        }
        Executor executor = (Executor) getTtlExecutor.invoke(ctx.executor());
        Proxy proxy = new Proxy();
        System.out.println(msg);
        CompletableFuture<Proxy> proxyCompletableFuture = CompletableFuture.completedFuture(proxy);
        proxyCompletableFuture.thenApplyAsync(resq -> ProxyHandler.builder(ctx, msg, proxy, this), executor)
                .thenApplyAsync(ProxyHandler::statistics, executor)
                .thenApplyAsync(ProxyHandler::filter, executor)
                .thenApplyAsync(ProxyHandler::proxyInfo, executor)
                .exceptionally(ProxyHandler::exception)
                .thenAcceptAsync(gateWayInfo -> ProxyHandler.endGateWay(proxyCompletableFuture, gateWayInfo, ctx, msg), ctx.channel().eventLoop());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        if (outboundChannel != null) {
            closeOnFlush(outboundChannel);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        closeOnFlush(ctx.channel());
    }
}
