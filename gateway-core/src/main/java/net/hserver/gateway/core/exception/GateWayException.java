package net.hserver.gateway.core.exception;

import io.netty.handler.codec.http.HttpResponseStatus;

/**
 * @author hxm
 */
public class GateWayException extends Exception {

    private HttpResponseStatus status;

    private Throwable throwable;

    public GateWayException(HttpResponseStatus status, String s) {
        super(s);
        this.status = status;
    }

    public GateWayException(HttpResponseStatus status, String s,Throwable throwable) {
        super(s);
        this.status = status;
        this.throwable=throwable;
    }

    public HttpResponseStatus getStatus() {
        return status;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
