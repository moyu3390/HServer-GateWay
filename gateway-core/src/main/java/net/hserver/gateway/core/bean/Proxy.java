package net.hserver.gateway.core.bean;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpRequest;
import lombok.Data;
import net.hserver.gateway.core.strategy.ServerNode;
import net.hserver.gateway.core.FrontendHandler;

/**
 * @author hxm
 */
@Data
public class Proxy {
  private Object msg;
  private ChannelHandlerContext ctx;
  private ServerNode serverNode;
  private FrontendHandler frontendHandler;
  private Throwable throwable;
  private Object request;
}
