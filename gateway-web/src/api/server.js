import axios from '@/libs/api.request'

export const restart = () => {
  return axios.request({
    url: '/v1/server/restart',
    method: 'get',
  })
};

export const  nowData = () => {
  return axios.request({
    url: '/v1/server/nowData',
    method: 'get',
  })
};
