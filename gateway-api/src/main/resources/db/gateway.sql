/*
 Navicat Premium Data Transfer

 Source Server         : gateway
 Source Server Type    : SQLite
 Source Server Version : 3030001
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3030001
 File Encoding         : 65001

 Date: 28/05/2021 16:42:15
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Table structure for gateway_node
-- ----------------------------
DROP TABLE IF EXISTS "gateway_node";
CREATE TABLE "gateway_node" (
  "id" text NOT NULL,
  "type" integer,
  "ip" TEXT,
  "port" integer,
  "weight" integer,
  "desc" TEXT,
  PRIMARY KEY ("id")
);

-- ----------------------------
-- Table structure for gateway_router
-- ----------------------------
DROP TABLE IF EXISTS "gateway_router";
CREATE TABLE "gateway_router" (
  "id" text NOT NULL,
  "url" TEXT,
  "node_ids" TEXT,
  "desc" TEXT,
  "domain" TEXT,
  "strategy_type" integer,
  PRIMARY KEY ("id")
);

-- ----------------------------
-- Table structure for gateway_router_node
-- ----------------------------
DROP TABLE IF EXISTS "gateway_router_node";
CREATE TABLE "gateway_router_node" (
  "id" text NOT NULL,
  "router_id" text,
  "node_id" text,
  PRIMARY KEY ("id")
);

-- ----------------------------
-- Table structure for gateway_statistics
-- ----------------------------
DROP TABLE IF EXISTS "gateway_statistics";
CREATE TABLE "gateway_statistics" (
  "id" text NOT NULL,
  "uv" text,
  "pv" text,
  "pc_platform" text,
  "android_platform" text,
  "ios_platform" text,
  "other_platform" text,
  "create_time" text,
  PRIMARY KEY ("id")
);

PRAGMA foreign_keys = true;
