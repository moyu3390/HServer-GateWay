package net.hserver.gateway;

import top.hserver.HServerApplication;
import top.hserver.core.ioc.annotation.HServerBoot;
import top.hserver.core.server.util.PropUtil;

import java.util.Properties;

/**
 * @author hxm
 */
@HServerBoot
public class GateWayStartApp {

    public static Integer webPort = 9999;
    public static Integer gateWayPort = 80;

    public static void main(String[] args) {
        PropUtil instance = PropUtil.getInstance();
        webPort = instance.getInt("webPort");
        gateWayPort = instance.getInt("gateWayPort");
        HServerApplication.run(GateWayStartApp.class, new Integer[]{webPort, gateWayPort}, args);
    }
}
