package net.hserver.gateway.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.SQLReady;
import org.beetl.sql.core.page.PageResult;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Bean;
import net.hserver.gateway.dao.GatewayRouterDao;
import net.hserver.gateway.dao.GatewayRouterNodeDao;
import net.hserver.gateway.dto.GatewayRouterDTO;
import net.hserver.gateway.dto.GatewayRouterNodeDTO;
import net.hserver.gateway.entity.GateWayRouter;
import net.hserver.gateway.entity.GateWayRouterNode;
import net.hserver.gateway.service.GatewayRouterService;
import net.hserver.gateway.utils.UUIDUtil;

import java.util.List;

/**
 * @author hxm
 */

@Slf4j
@Bean
public class GatewayRouterServiceImpl implements GatewayRouterService {

    @Autowired
    private GatewayRouterDao gatewayRouterDao;

    @Autowired
    private GatewayRouterNodeDao gatewayRouterNodeDao;

    @Autowired
    private SQLManager sqlManager;


    @Override
    public String add(GatewayRouterDTO gatewayRouterDTO) {

        String id1 = UUIDUtil.randomUUID32();
        GateWayRouter gateWayRouter = new GateWayRouter();
        gateWayRouter.setDesc(gatewayRouterDTO.getDesc());
        gateWayRouter.setUrl(gatewayRouterDTO.getUrl());
        gateWayRouter.setId(id1);
        gateWayRouter.setNodeIds(gatewayRouterDTO.getNodeIds());
        gateWayRouter.setStrategyType(gatewayRouterDTO.getStrategyType());
        gatewayRouterDao.insert(gateWayRouter);

        String[] split = gatewayRouterDTO.getNodeIds().split(",");
        for (String s : split) {
            GateWayRouterNode gateWayRouterNode = new GateWayRouterNode();
            gateWayRouterNode.setId(UUIDUtil.randomUUID32());
            gateWayRouterNode.setNodeId(s);
            gateWayRouterNode.setRouterId(id1);
            gatewayRouterNodeDao.insert(gateWayRouterNode);
        }
        return id1;
    }


    @Override
    public PageResult list(Long page, Long pageSize) {
        return gatewayRouterDao.createLambdaQuery().page(page, pageSize);
    }


    @Override
    public List<GatewayRouterNodeDTO> all() {
        List<GatewayRouterNodeDTO> execute = sqlManager.execute(new SQLReady("SELECT  gateway_router.strategy_type, weight, type,node_id,router_id,url,ip,port,gateway_router.desc as router_desc,gateway_node.desc as node_desc FROM gateway_router LEFT JOIN gateway_router_node ON gateway_router.id=gateway_router_node.router_id LEFT JOIN gateway_node ON gateway_node.id=gateway_router_node.node_id"), GatewayRouterNodeDTO.class);
        return execute;
    }

    @Override
    public List<GatewayRouterNodeDTO> getOne(String id) {
        return sqlManager.execute(new SQLReady("SELECT  gateway_router.strategy_type, weight,type,node_id,router_id,url,ip,port,gateway_router.desc as router_desc,gateway_node.desc as node_desc FROM gateway_router LEFT JOIN gateway_router_node ON gateway_router.id=gateway_router_node.router_id LEFT JOIN gateway_node ON gateway_node.id=gateway_router_node.node_id where gateway_router.id= '" + id + "'"), GatewayRouterNodeDTO.class);
    }

    @Override
    public boolean update(GatewayRouterDTO gatewayRouterDTO) {
        //更新路由
        GateWayRouter gateWayRouter = new GateWayRouter();
        gateWayRouter.setDesc(gatewayRouterDTO.getDesc());
        gateWayRouter.setUrl(gatewayRouterDTO.getUrl());
        gateWayRouter.setId(gatewayRouterDTO.getId());
        gateWayRouter.setNodeIds(gatewayRouterDTO.getNodeIds());
        gateWayRouter.setStrategyType(gatewayRouterDTO.getStrategyType());
        gatewayRouterDao.updateById(gateWayRouter);

        //删除依赖，重新来
        gatewayRouterNodeDao.createLambdaQuery().andEq(GateWayRouterNode::getRouterId, gatewayRouterDTO.getId()).delete();

        String[] split = gatewayRouterDTO.getNodeIds().split(",");
        for (String s : split) {
            GateWayRouterNode gateWayRouterNode = new GateWayRouterNode();
            gateWayRouterNode.setId(UUIDUtil.randomUUID32());
            gateWayRouterNode.setNodeId(s);
            gateWayRouterNode.setRouterId(gatewayRouterDTO.getId());
            gatewayRouterNodeDao.insert(gateWayRouterNode);
        }
        return true;
    }

    @Override
    public boolean remove(String id) {
        gatewayRouterDao.deleteById(id);
        gatewayRouterNodeDao.createLambdaQuery().andEq(GateWayRouterNode::getRouterId, id).delete();
        return true;
    }
}
