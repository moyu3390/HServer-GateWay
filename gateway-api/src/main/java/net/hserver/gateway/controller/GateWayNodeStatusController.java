package net.hserver.gateway.controller;

import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Controller;
import top.hserver.core.ioc.annotation.GET;
import top.hserver.core.server.util.JsonResult;
import net.hserver.gateway.core.node.Manager;
import net.hserver.gateway.core.strategy.ServerNode;

import java.util.List;

/**
 * 服务节点管理
 *
 * @author hxm
 */
@Controller(value = "/v1/serverNodeStatus/", name = "服务节点管理 ")
public class GateWayNodeStatusController {

  @Autowired
  private Manager manager;

  @GET("all")
  public JsonResult all() {
    List<ServerNode> allServerNode = manager.getAllServerNode();
    return JsonResult.ok().put("data",allServerNode);
  }

}
