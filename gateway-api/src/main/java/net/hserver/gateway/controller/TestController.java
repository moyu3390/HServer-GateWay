package net.hserver.gateway.controller;
import top.hserver.core.interfaces.HttpRequest;
import top.hserver.core.ioc.annotation.Controller;
import top.hserver.core.ioc.annotation.GET;
import top.hserver.core.server.util.JsonResult;

@Controller
public class TestController {

  @GET("/test")
  public JsonResult test(HttpRequest request) {
    return JsonResult.ok();
  }

}
