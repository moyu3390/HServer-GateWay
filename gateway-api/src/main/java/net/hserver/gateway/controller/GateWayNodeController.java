package net.hserver.gateway.controller;

import net.hserver.gateway.service.GatewayNodeService;
import top.hserver.core.ioc.annotation.Autowired;
import top.hserver.core.ioc.annotation.Controller;
import top.hserver.core.ioc.annotation.GET;
import top.hserver.core.ioc.annotation.POST;
import top.hserver.core.server.util.JsonResult;
import net.hserver.gateway.entity.GateWayNode;

/**
 * 服务节点管理
 *
 * @author hxm
 */
@Controller(value = "/v1/serverNode/", name = "服务节点管理 ")
public class GateWayNodeController {

  @Autowired
  private GatewayNodeService gatewayNodeService;


  @POST("add")
  public JsonResult add(GateWayNode gateWayNode) {
    if (gatewayNodeService.add(gateWayNode)) {
      return JsonResult.ok();
    } else {
      return JsonResult.error();
    }
  }

  @POST("update")
  public JsonResult update(GateWayNode gateWayNode) {
    if (gatewayNodeService.update(gateWayNode)) {
      return JsonResult.ok();
    } else {
      return JsonResult.error();
    }
  }

  @GET("list")
  public JsonResult list(Long page, Long pageSize) {
    return JsonResult.ok().put("data", gatewayNodeService.list(page, pageSize));
  }

  @GET("all")
  public JsonResult all() {
    return JsonResult.ok().put("data", gatewayNodeService.all());
  }

  @GET("remove")
  public JsonResult remove(String id) {
    if (gatewayNodeService.remove(id)) {
      return JsonResult.ok();
    } else {
      return JsonResult.error();
    }
  }
}
