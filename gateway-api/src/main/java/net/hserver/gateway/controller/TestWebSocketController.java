package net.hserver.gateway.controller;

import top.hserver.core.interfaces.HttpRequest;
import top.hserver.core.interfaces.WebSocketHandler;
import top.hserver.core.ioc.annotation.Controller;
import top.hserver.core.ioc.annotation.GET;
import top.hserver.core.ioc.annotation.WebSocket;
import top.hserver.core.server.handlers.Ws;
import top.hserver.core.server.util.JsonResult;

@WebSocket("/ws")
public class TestWebSocketController implements WebSocketHandler {

    @Override
    public void onConnect(Ws ws) {
        ws.send("666");
        System.out.println("连接："+ws.getUid());
    }

    @Override
    public void onMessage(Ws ws) {
        System.out.println("消息:"+ws.getMessage());
    }

    @Override
    public void disConnect(Ws ws) {
        System.out.println("断开："+ws.getUid());
    }
}
