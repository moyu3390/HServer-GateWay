package net.hserver.gateway.dispatch;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import net.hserver.gateway.GateWayStartApp;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.ioc.annotation.Order;
import top.hserver.core.server.dispatcher.DispatchHttp;

import java.net.InetSocketAddress;

@Order(3)
@Bean
public class DispatchGateWayServerHttp extends DispatchHttp {

    @Override
    public boolean dispatcher(ChannelHandlerContext ctx, ChannelPipeline pipeline, byte[] headers) {
        InetSocketAddress socketAddress = (InetSocketAddress) ctx.channel().localAddress();
        int port = socketAddress.getPort();
        if (port != GateWayStartApp.webPort) {
            return false;
        }
        return super.dispatcher(ctx, pipeline, headers);
    }
}
