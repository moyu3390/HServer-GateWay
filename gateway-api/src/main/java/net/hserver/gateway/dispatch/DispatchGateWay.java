package net.hserver.gateway.dispatch;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import net.hserver.gateway.GateWayStartApp;
import net.hserver.gateway.core.FrontendHandler;
import top.hserver.core.interfaces.ProtocolDispatcherAdapter;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.ioc.annotation.Order;
import top.hserver.core.server.context.ConstConfig;

import java.net.InetSocketAddress;

@Order(1)
@Bean
public class DispatchGateWay implements ProtocolDispatcherAdapter {

    public boolean dispatcher(ChannelHandlerContext ctx, ChannelPipeline pipeline, byte[] headers) {
        InetSocketAddress socketAddress = (InetSocketAddress) ctx.channel().localAddress();
        int port = socketAddress.getPort();
        if (port != GateWayStartApp.gateWayPort) {
            return false;
        }
        pipeline.addLast(ConstConfig.BUSINESS_EVENT,new HttpServerCodec());
        pipeline.addLast(ConstConfig.BUSINESS_EVENT,new HttpObjectAggregator(Integer.MAX_VALUE));
        pipeline.addLast(ConstConfig.BUSINESS_EVENT,new FrontendHandler());
        return true;
    }
}
