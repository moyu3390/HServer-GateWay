package net.hserver.gateway.task;

import lombok.extern.slf4j.Slf4j;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.ioc.annotation.Task;

/**
 * @author hxm
 */
@Bean
@Slf4j
public class ServerNodeCheckTask {

    @Task(name = "ServerNodeCheckTask", time = "1000000")
    public void checkTask() {
        //结果
        log.info("我在检查服务节点状态，当正常时，自动拉回");
    }

}
