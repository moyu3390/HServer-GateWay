<p align="center">    
    <h1 align="center">HServer-GateWay</h1>
</p>
<p align="center">
    <a >
        <img src="https://img.shields.io/badge/Build-Java8-red.svg?style=flat" />
    </a>
     <a >
          <img src="https://img.shields.io/badge/HServer-2.9.26-yeoll.svg?style=flat" />
      </a>
    <a >
        <img src="https://img.shields.io/badge/Netty-4.1.49.Final-blue.svg" alt="flat">
    </a>
    <a >
        <img src="https://img.shields.io/badge/Licence-Apache2.0-green.svg?style=flat" />
    </a>

</p>
<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>

#### 介绍
基于Netty的高性能网关

````
代理性能测试，Win10,8h 8W QPS
代理性能测试，Linux,8h 18W QPS

代理逻辑。80端口（网关）->9999（后台服务HServer可达23QPS） /test

[root@localhost ~]# ab -c100 -n100000 -k http://127.0.0.1/test
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient)
Completed 10000 requests
Completed 20000 requests
Completed 30000 requests
Completed 40000 requests
Completed 50000 requests
Completed 60000 requests
Completed 70000 requests
Completed 80000 requests
Completed 90000 requests
Completed 100000 requests
Finished 100000 requests


Server Software:        2.9.21
Server Hostname:        127.0.0.1
Server Port:            80

Document Path:          /test
Document Length:        33 bytes

Concurrency Level:      100
Time taken for tests:   0.552 seconds
Complete requests:      100000
Failed requests:        0
Write errors:           0
Keep-Alive requests:    100000
Total transferred:      17600000 bytes
HTML transferred:       3300000 bytes
Requests per second:    181206.36 [#/sec] (mean)
Time per request:       0.552 [ms] (mean)
Time per request:       0.006 [ms] (mean, across all concurrent requests)
Transfer rate:          31144.84 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       2
Processing:     0    1   1.1      0      38
Waiting:        0    1   1.1      0      38
Total:          0    1   1.1      0      38

Percentage of the requests served within a certain time (ms)
  50%      0
  66%      0
  75%      1
  80%      1
  90%      1
  95%      1
  98%      1
  99%      2
 100%     38 (longest request)

````

#### 效果图

<p align="center">    
    <img src="https://gitee.com/HServer/HServer-GateWay/raw/master/doc/img/a.png" />
    <img src="https://gitee.com/HServer/HServer-GateWay/raw/master/doc/img/b.png" />
    <img src="https://gitee.com/HServer/HServer-GateWay/raw/master/doc/img/c.png" />
</p>    